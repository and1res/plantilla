(function(){
    var personaje;
    var bmas, bmenos;
    var escala = 1;
    var paso = 20;

    function cambiarEscala(opc){
    	escala = opc?escala+paso:escala-paso;
    	// personaje.css("-webkit-transform","scale("+escala+")");
    	paso+=paso;
    	TweenLite.to(personaje,
    				 .5,
    				 {css:{x:paso},ease:Bounce.easeOut});
    }
    
    window.onload = function()
    {
    	personaje = $("#personaje");
    	bmas = $("#btnMas");
    	bmenos = $("#btnMenos");
    	bmas.click(function(){
    		cambiarEscala(true);
    	});
    	bmenos.click(function(){
    		cambiarEscala(false);
    	});
    }
})();